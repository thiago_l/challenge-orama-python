# API Documentation

## Summary
* [API Status](#api-status)
* [Freelancer Skills](#freelancer-skills)

## API Status

To check the API status just send a **GET** request to ```localhost:5000/```.

Response: ```200 OK```

```json
{
    "message": "API is Running"
}
```
    
## Freelancer Skills:

Get the Freelancer computed skills. 

URL: ```localhost:5000/freelancers/computed-skills```
   
Method: ```POST```
    
Payload Example:
```json
{
  "freelance": {
    "id": 42,
    "user": {
      "firstName": "Hunter",
      "lastName": "Moore",
      "jobTitle": "Fullstack JS Developer"
    },
    "status": "new",
    "retribution": 650,
    "availabilityDate": "2018-06-13T00:00:00+01:00",
    "professionalExperiences": [
      {
        "id": 4,
        "companyName": "Okuneva, Kerluke and Strosin",
        "startDate": "2016-01-01T00:00:00+01:00",
        "endDate": "2018-05-01T00:00:00+01:00",
        "skills": [
          {
            "id": 241,
            "name": "React"
          },
          {
            "id": 270,
            "name": "Node.js"
          },
          {
            "id": 370,
            "name": "Javascript"
          }
        ]
      },
      {
        "id": 54,
        "companyName": "Hayes - Veum",
        "startDate": "2014-01-01T00:00:00+01:00",
        "endDate": "2016-09-01T00:00:00+01:00",
        "skills": [
          {
            "id": 470,
            "name": "MySQL"
          },
          {
            "id": 400,
            "name": "Java"
          },
          {
            "id": 370,
            "name": "Javascript"
          }
        ]
      },
      {
        "id": 80,
        "companyName": "Harber, Kirlin and Thompson",
        "startDate": "2013-05-01T00:00:00+01:00",
        "endDate": "2014-07-01T00:00:00+01:00",
        "skills": [
          {
            "id": 370,
            "name": "Javascript"
          },
          {
            "id": 400,
            "name": "Java"
          }
        ]
      }
    ]
  }
} 
```

Success Response: ```200 OK```
    
```json
{
    "freelance": {
    
        "id": 42,
        "computedSkills": [
            {
                "id": 241,
                "name": "React",
                "durationInMonths": 28
            },
            {
                "id": 270,
                "name": "Node.js",
                "durationInMonths": 28
            },
            {
                "id": 370,
                "name": "Javascript",
                "durationInMonths": 60
            },
            {
                "id": 470,
                "name": "MySQL",
                "durationInMonths": 32
            },
            {
                "id": 400,
                "name": "Java",
                "durationInMonths": 40
            }
        ]
    }
}
```

Error Response: ```422 UNPROCESSABLE ENTITY```   
 
```html
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<title>422 Unprocessable Entity</title>
<h1>Unprocessable Entity</h1>
<p>The request was well-formed but was unable to be followed due to semantic errors.</p>
```
> This error response occurs when the payload data has invalid format or semantic errors.