# Challenge Órama Python

[![Python Version](https://img.shields.io/badge/python-3.7-brightgreen.svg)](https://python.org)
[![Flask Version](https://img.shields.io/badge/flask-1.1.2-brightgreen.svg)](https://flask.palletsprojects.com/en/1.1.x/)

Project to process the Freelancer data and return your skills with the experience time in months.

## Running the Project Locally

1.Download or clone the repository to your local machine:
```bash 
git clone <repository_url>
```

2.Create the Virtual environment ``virtualenv`` :

```bash
virtualenv venv
```
    
3.Activate the Virtual environment ``virtualenv``:
    
```bash
venv/bin/activate
```

4.Install the requirements:

```bash
pip install -r requirements.txt
```
    
5.Finally, run the development server:

```bash 
python app.py
```

## Running the Project Tests

1.To run the project unit test just execute:
```bash 
py.test
```

## Documentation

* [API Documentation](api/README.md)