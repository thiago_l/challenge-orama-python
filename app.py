import flask
from flask import request, abort, jsonify

from api.models.freelancer import Freelancer

app = flask.Flask(__name__)
app.config["DEBUG"] = True
app.config['JSON_SORT_KEYS'] = False


@app.route('/', methods=['GET'])
def welcome():
    return jsonify({"message": "API is Running"})


@app.route('/freelancers/computed-skills', methods=['POST'])
def freelancer_skills():
    json = request.get_json()
    try:
        freelancer = Freelancer(**json['freelance'])
    except:
        abort(422)
    return jsonify({"freelance": freelancer.as_json()})


if __name__ == '__main__':
   app.run()
