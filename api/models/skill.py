from dateutil.relativedelta import relativedelta


class Skill:

    def __init__(self, id, name, start_date, end_date):
        self.id = id
        self.name = name
        self.duration_months = set()
        self.start_date = start_date
        self.end_date = end_date

        self.get_experience_duration()

    def get_experience_duration(self):
        curent_date = self.start_date
        while curent_date < self.end_date:
            self.duration_months.add(curent_date)
            curent_date += relativedelta(months=1)

    def update_experience_duration(self):
        curent_date = self.start_date
        set_temp = set()
        while curent_date < self.end_date:
            set_temp.add(curent_date)
            curent_date += relativedelta(months=1)
        self.duration_months.update(set_temp)

    def get_duration_months(self):
        return len(self.duration_months)

    def as_json(self):
        return dict(
            id=self.id,
            name=self.name,
            durationInMonths=self.get_duration_months()
        )
