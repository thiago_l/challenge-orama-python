from datetime import datetime

from api.models.skill import Skill


class Freelancer:

    def __init__(self, **kwargs):
        self.__dict__.update(kwargs)
        self.professional_skills = dict()

        self.get_professional_experiences()

    def get_professional_experiences(self):
        for pe in self.professionalExperiences:
            start_date = datetime.fromisoformat(pe['startDate'])
            end_date = datetime.fromisoformat(pe['endDate'])

            for skill in pe['skills']:
                if skill['id'] in self.professional_skills.keys():
                    sk = self.professional_skills[skill['id']]
                    sk.start_date = start_date
                    sk.end_date = end_date
                    sk.update_experience_duration()
                else:
                    self.professional_skills[skill['id']] = Skill(
                        skill['id'],
                        skill['name'],
                        start_date,
                        end_date
                    )

    def as_json(self):
        return dict(
            id=self.id,
            computedSkills=[obj.as_json() for obj in self.professional_skills.values()]
        )
