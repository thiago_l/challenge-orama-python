import unittest

from api.models.freelancer import Freelancer


class TestFreelancer(unittest.TestCase):
    def setUp(self):
        self.data = {
            "id": 42,
            "professionalExperiences": [
              {
                "startDate": "2016-01-01T00:00:00+01:00",
                "endDate": "2018-05-01T00:00:00+01:00",
                "skills": [
                  {
                    "id": 241,
                    "name": "React"
                  },
                  {
                    "id": 270,
                    "name": "Node.js"
                  },
                  {
                    "id": 370,
                    "name": "Javascript"
                  }
                ]
              },
              {
                "startDate": "2014-01-01T00:00:00+01:00",
                "endDate": "2016-09-01T00:00:00+01:00",
                "skills": [
                  {
                    "id": 470,
                    "name": "MySQL"
                  },
                  {
                    "id": 400,
                    "name": "Java"
                  },
                  {
                    "id": 370,
                    "name": "Javascript"
                  }
                ]
              },
              {
                "startDate": "2013-05-01T00:00:00+01:00",
                "endDate": "2014-07-01T00:00:00+01:00",
                "skills": [
                  {
                    "id": 370,
                    "name": "Javascript"
                  },
                  {
                    "id": 400,
                    "name": "Java"
                  }
                ]
              }
            ]
          }

        self.no_overlap_data = {
            "id": 42,
            "professionalExperiences": [
              {
                "startDate": "2019-01-01T00:00:00+01:00",
                "endDate": "2019-05-01T00:00:00+01:00",
                "skills": [
                  {
                    "id": 370,
                    "name": "Javascript"
                  }
                ]
              },
              {
                "startDate": "2019-08-01T00:00:00+01:00",
                "endDate": "2020-01-01T00:00:00+01:00",
                "skills": [
                  {
                    "id": 370,
                    "name": "Javascript"
                  }
                ]
              }
            ]
          }

        self.overlap_data = {
            "id": 42,
            "professionalExperiences": [
              {
                "startDate": "2016-01-01T00:00:00+01:00",
                "endDate": "2018-05-01T00:00:00+01:00",
                "skills": [
                  {
                    "id": 400,
                    "name": "Java"
                  }
                ]
              },
              {
                "startDate": "2014-01-01T00:00:00+01:00",
                "endDate": "2016-09-01T00:00:00+01:00",
                "skills": [
                  {
                    "id": 400,
                    "name": "Java"
                  }
                ]
              },
              {
                "startDate": "2013-05-01T00:00:00+01:00",
                "endDate": "2014-07-01T00:00:00+01:00",
                "skills": [
                  {
                    "id": 400,
                    "name": "Java"
                  }
                ]
              }
            ]
          }

    def test_freelancer(self):
        freelancer = Freelancer(**self.data)
        # JAVA
        self.assertEqual(40, freelancer.professional_skills.get(400).get_duration_months())
        # JAVA SCRIPT
        self.assertEqual(60, freelancer.professional_skills.get(370).get_duration_months())
        # MYSQL
        self.assertEqual(32, freelancer.professional_skills.get(470).get_duration_months())
        # NODE.JS
        self.assertEqual(28, freelancer.professional_skills.get(270).get_duration_months())
        # REACT
        self.assertEqual(28, freelancer.professional_skills.get(241).get_duration_months())

    def test_freelancer_no_overlap(self):
        freelancer = Freelancer(**self.no_overlap_data)
        self.assertEqual(9, freelancer.professional_skills.get(370).get_duration_months())

    def test_freelancer_overlap(self):
        freelancer = Freelancer(**self.overlap_data)
        self.assertEqual(60, freelancer.professional_skills.get(400).get_duration_months())


if __name__ == '__main__':
    unittest.main()
