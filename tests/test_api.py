import json
import unittest

from app import app


class TestApiStatus(unittest.TestCase):
    def setUp(self):
        my_app = app.test_client()
        self.response = my_app.get('/')

    def test_get_status_code(self):
        self.assertEqual(200, self.response.status_code)

    def test_if_index_returns_ok_string(self):
        self.assertEqual("API is Running", self.response.get_json()['message'])

    def test_content_type(self):
        self.assertIn('application/json', self.response.content_type)


class TestApiFreelancerValidCase(unittest.TestCase):
    def setUp(self):
        data = {
          "freelance": {
            "id": 42,
            "user": {
              "firstName": "Hunter",
              "lastName": "Moore",
              "jobTitle": "Fullstack JS Developer"
            },
            "status": "new",
            "retribution": 650,
            "availabilityDate": "2018-06-13T00:00:00+01:00",
            "professionalExperiences": [
              {
                "id": 4,
                "companyName": "Okuneva, Kerluke and Strosin",
                "startDate": "2016-01-01T00:00:00+01:00",
                "endDate": "2018-05-01T00:00:00+01:00",
                "skills": [
                  {
                    "id": 241,
                    "name": "React"
                  },
                  {
                    "id": 270,
                    "name": "Node.js"
                  },
                  {
                    "id": 370,
                    "name": "Javascript"
                  }
                ]
              },
              {
                "id": 54,
                "companyName": "Hayes - Veum",
                "startDate": "2014-01-01T00:00:00+01:00",
                "endDate": "2016-09-01T00:00:00+01:00",
                "skills": [
                  {
                    "id": 470,
                    "name": "MySQL"
                  },
                  {
                    "id": 400,
                    "name": "Java"
                  },
                  {
                    "id": 370,
                    "name": "Javascript"
                  }
                ]
              },
              {
                "id": 80,
                "companyName": "Harber, Kirlin and Thompson",
                "startDate": "2013-05-01T00:00:00+01:00",
                "endDate": "2014-07-01T00:00:00+01:00",
                "skills": [
                  {
                    "id": 370,
                    "name": "Javascript"
                  },
                  {
                    "id": 400,
                    "name": "Java"
                  }
                ]
              }
            ]
          }
        }
        my_app = app.test_client()
        self.response = my_app.post('/freelancers/computed-skills',
                                    headers={"Content-Type": "application/json"},
                                    data=json.dumps(data))

    def test_post_status_code(self):
        self.assertEqual(200, self.response.status_code)

    def test_returns(self):
        json = self.response.get_json()
        count = sum(skill['durationInMonths'] for skill in json['freelance']['computedSkills'])
        self.assertEqual(188, count)

    def test_content_type(self):
        self.assertIn('application/json', self.response.content_type)


class TestApiFreelancerInvalidCase(unittest.TestCase):
    def setUp(self):
        data = {
          "freelance": {
            "id": 42,
            "user": {
              "firstName": "Hunter",
              "lastName": "Moore",
              "jobTitle": "Fullstack JS Developer"
            }
          }
        }
        my_app = app.test_client()
        self.response = my_app.post('/freelancers/computed-skills',
                                    headers={"Content-Type": "application/json"},
                                    data=json.dumps(data))

    def test_post_status_code(self):
        self.assertEqual(422, self.response.status_code)

    def test_content_type(self):
        self.assertIn('text/html', self.response.content_type)


if __name__ == '__main__':
    unittest.main()
